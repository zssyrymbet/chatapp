////
////  ExtentionTextView.swift
////  сhatApplication
////
////  Created by Zarina Syrymbet on 4/8/20.
////  Copyright © 2020 Zarina Syrymbet. All rights reserved.
////
//
//import UIKit
//
//extension ViewController: UITextViewDelegate {
//    
//    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
//        
//        let currentText: NSString = email.text
//        let updatedText = currentText.stringByReplacingCharactersInRange(range, withString:text)
//        
//        if updatedText.isEmpty {
//            textView.text = placeholder
//            textView.textColor = UIColor.lightGrayColor()
//            textView.selectedTextRange = textView.textRangeFromPosition(textView.beginningOfDocument, toPosition: textView.beginningOfDocument)
//            return false
//        }
//        else if textView.textColor == UIColor.lightGrayColor() && !text.isEmpty {
//            textView.text = nil
//            textView.textColor = UIColor.blackColor()
//        }
//        
//        return true
//    }
//    
//    func textViewDidChangeSelection(textView: UITextView) {
//        if self.view.window != nil {
//            if textView.textColor == UIColor.lightGrayColor() {
//                textView.selectedTextRange = textView.textRangeFromPosition(textView.beginningOfDocument, toPosition: textView.beginningOfDocument)
//            }
//        }
//    }
//    
//}
