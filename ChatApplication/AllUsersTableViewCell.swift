//
//  AllUsersTableViewCell.swift
//  сhatApplication
//
//  Created by Zarina Syrymbet on 4/8/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class AllUsersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var status: UILabel!
    
    func configureCell(user: User){
        print("USER: ", user)
        email.text = user.email
        username.text = user.username
        
        if user.status == true {
            status.text = "Online"
        }
    }
    
}
