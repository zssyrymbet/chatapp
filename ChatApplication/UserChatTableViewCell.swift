//
//  UserChatTableViewCell.swift
//  ChatApplication
//
//  Created by Zarina Syrymbet on 4/7/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class UserChatTableViewCell: UITableViewCell {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var message: UITextView!
    private let manager = ChatManager()

    func configureUserCell(chat: Chat){
        for user in chat.participantIds {
            if user != Auth.auth().currentUser!.uid {
                manager.getUserById(userId: user) { (user) in
                    if user != nil {
                        self.username.text = user?.username
                        self.message.text = chat.lastMessage
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MMM d, h:mm a"
                        self.date.text = dateFormatter.string(from: chat.lastMessageTimestamp.dateValue())
                    }
                }
            }
        }
    }
}

