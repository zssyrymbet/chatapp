//
//  MessageCurrentTableViewCell.swift
//  сhatApplication
//
//  Created by Zarina Syrymbet on 4/16/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class MessageCurrentTableViewCell: UITableViewCell {

    @IBOutlet weak var currentUserMessage: UILabel!
    @IBOutlet weak var currentUserDate: UILabel!
    
    func configureCurrentMessageCell(message: Message) {
        currentUserMessage.text = message.message
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        currentUserDate.text = dateFormatter.string(from: message.timestamp.dateValue())
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 7, right: 0))
    }

}
