//
//  Chat.swift
//  сhatApplication
//
//  Created by Zarina Syrymbet on 4/8/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import FirebaseFirestore

struct Chat {
    var id: String
    var participantIds: [String]
    var participants: [User?]
    var lastMessage: String
    var lastMessageTimestamp: Timestamp
    
    init?(data: [String: Any]) {
        guard let id = data["id"] as? String, let participantIds = data["participantIds"] as? [String], let participants = data["participants"] as? [[String: Any]],
            let lastMessage = data["lastMessage"] as? String,
            let lastMessageTimestamp = data["lastMessageTimestamp"] as? Timestamp else {
                    return nil
            }
        
            self.id = id
            self.participantIds = participantIds
            self.participants = participants.map({ User(data: $0) })
            self.lastMessage = lastMessage
            self.lastMessageTimestamp = lastMessageTimestamp
    }
}

