//
//  SignUpViewController.swift
//  ChatApplication
//
//  Created by Zarina Syrymbet on 4/7/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    private let manager = ChatManager()
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var label: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func SignUpButtonPressed(_ sender: Any) {
        manager.createUser(email: email.text!, password: password.text!, username: username.text!) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
