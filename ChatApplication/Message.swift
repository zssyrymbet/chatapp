//
//  Message.swift
//  сhatApplication
//
//  Created by Zarina Syrymbet on 4/8/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import FirebaseFirestore

struct Message {
    var chatId: String
    var senderId: String
    var message: String
    var timestamp: Timestamp
    
    init?(data: [String: Any]) {
        guard let chatId = data["chatId"] as? String, let senderId = data["senderId"] as? String, let message = data["message"] as? String, let timestamp = data["timestamp"] as?    Timestamp else {
                return nil
            }
        
            self.chatId = chatId
            self.senderId = senderId
            self.message = message
            self.timestamp = timestamp
    }
}
