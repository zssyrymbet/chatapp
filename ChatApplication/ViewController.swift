//
//  ViewController.swift
//  ChatApplication
//
//  Created by Zarina Syrymbet on 4/6/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    private let manager = ChatManager()
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userID = Auth.auth().currentUser?.uid
        if(userID != nil){
            self.performSegue(withIdentifier: "UserChatViewController", sender: nil)
        }
    }

    @IBAction func SignUpButtonPressed(_ sender: Any) {
        func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if(segue.identifier == "SignUpViewController") {
                
            }
        }
    }
    
    @IBAction func LogInButtonPressed(_ sender: Any) {
        manager.signIn(email: email.text!, password: password.text!) {
            self.manager.updateStatus(userId: Auth.auth().currentUser!.uid, status: true)
            self.performSegue(withIdentifier: "UserChatViewController", sender: nil)
        }
    }
    
    
}



