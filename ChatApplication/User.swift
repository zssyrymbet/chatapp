//
//  User.swift
//  сhatApplication
//
//  Created by Zarina Syrymbet on 4/8/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import FirebaseFirestore

struct User {
    var id: String
    var username: String
    var email: String
    var password: String
    var status: Bool
    
    init?(data: [String: Any]) {
        guard let id = data["id"] as? String, let username = data["username"] as? String,
            let email = data["email"] as? String, let password = data["password"] as? String,
            let status = data["status"] as? Bool else {
                return nil
            }
        
            self.id = id
            self.username = username
            self.email = email
            self.password = password
            self.status = status
    }
    
    func toDict() -> [String: Any] {
        return [
            "id": id,
            "email": email,
            "username": username,
            "password": password,
            "status": status,
        ]
    }
}
