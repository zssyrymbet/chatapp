//
//  ChatViewController.swift
//  ChatApplication
//
//  Created by Zarina Syrymbet on 4/7/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class ChatViewController: UIViewController,  UITableViewDataSource,  UITableViewDelegate {
    
    private let manager = ChatManager()
    var userList: [User?] = []
    var cellValue: User?
    var cellValueChat: Chat?
    var messageList: [Message?] = []
    @IBOutlet weak var messageEnter: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 20
            
        if cellValue != nil {
            self.title = cellValue?.username
        } else {
            if cellValueChat != nil {
                for user in cellValueChat!.participantIds {
                    if user != Auth.auth().currentUser!.uid {
                        manager.getUserById(userId: user) { (user) in
                            self.title = user?.username
                        }
                    }
                }
            }
        }
        
        manager.getUsers { (users) in
            self.userList = users
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if cellValueChat != nil {
            loadMessages(chatId: cellValueChat!.id)
        }
    }
    
    private func loadMessages(chatId: String) {
        manager.getMessage(chatId: chatId) { (messages) in
            self.messageList = messages
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
        view.backgroundColor = .clear
        return view
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if messageList[indexPath.row]?.senderId == Auth.auth().currentUser!.uid {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCurrentCell", for: indexPath) as! MessageCurrentTableViewCell
                cell.configureCurrentMessageCell(message: messageList[indexPath.row]!)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageTableViewCell
            cell.configureMessageCell(message: messageList[indexPath.row]!)
            return cell
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        if (cellValueChat == nil) {
            manager.createChat(participantIds: [cellValue!.id, Auth.auth().currentUser!.uid], participants: userList, lastMessage: messageEnter.text!) { id in
                self.loadMessages(chatId: id)
                self.manager.createMessage(chatId: id, senderId: Auth.auth().currentUser!.uid, message: self.messageEnter.text!)
                self.messageEnter.text = nil
            }
        } else {
            manager.updateChat(chatId: cellValueChat!.id, lastMessage: messageEnter.text!)
            self.manager.createMessage(chatId: cellValueChat!.id, senderId: Auth.auth().currentUser!.uid, message: self.messageEnter.text!)
            self.messageEnter.text = nil
        }
        self.loadMessages(chatId: cellValueChat!.id)
    }
}

