//
//  AllUsersViewController.swift
//  сhatApplication
//
//  Created by Zarina Syrymbet on 4/8/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class AllUsersViewController: UITableViewController {
    private let manager = ChatManager()
    var userList: [User?] = []
    var chatList: [Chat?] = []

    override func viewDidLoad() {
        manager.getCurrentUserChats(currentUserId: Auth.auth().currentUser!.uid) { (chats) in
            if (chats.count != 0) {
                self.chatList = chats
            }
        }
        super.viewDidLoad()
    }
           
    override func viewDidAppear(_ animated: Bool) {
        manager.getUsers { (users) in
            self.userList = users
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
       
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllUsersCell", for: indexPath) as! AllUsersTableViewCell
                cell.configureCell(user: userList[indexPath.row]!)
                return cell
    }
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "UserChatViewController") {
            if let viewController = segue.destination as? ChatViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                    if chatList.isEmpty {
                        viewController.cellValue = userList[index]
                    } else {
                        var foundChat: Chat? = nil
                            for chat in chatList {
                                if chat!.participantIds.contains(userList[index]!.id) && chat!.participantIds.contains(Auth.auth().currentUser!.uid){
                                    foundChat = chat
                                    break
                                }
                            }
                            
                            if foundChat != nil {
                                viewController.cellValueChat = foundChat
                            } else {
                                viewController.cellValue = userList[index]
                            }
                    }
               }
        }
    }
}
