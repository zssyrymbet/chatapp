//
//  UserDataViewController.swift
//  ChatApplication
//
//  Created by Zarina Syrymbet on 4/7/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class UserDataViewController: UITableViewController {
    private let manager = ChatManager()
    var chatList: [Chat?] = []

    override func viewDidLoad() {
        manager.getUserById(userId: Auth.auth().currentUser!.uid) { (user) in
            self.title = user?.username
        }
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        manager.getCurrentUserChats(currentUserId: Auth.auth().currentUser!.uid) { (chats) in
            if (chats.count != 0) {
                self.chatList = chats
                self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserChatTableViewCell
            cell.configureUserCell(chat: chatList[indexPath.row]!)
            return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "UserChatViewController") {
            if let viewController = segue.destination as? ChatViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValueChat = chatList[index]
            }
        }
    }
    
    @IBAction func exit(_ sender: Any) {
        print("UPDATE",  Auth.auth().currentUser!.uid)
        self.manager.updateStatus(userId: Auth.auth().currentUser!.uid, status: false)
        if(manager.signOut()) {
            navigationController?.popViewController(animated: true)
        }
    }
}

