//
//  ChatManager.swift
//  сhatApplication
//
//  Created by Zarina Syrymbet on 4/8/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class ChatManager {

    func createUser(email: String, password: String, username: String,  onComplete: @escaping() -> Void) {
       Auth.auth().createUser(withEmail:email, password: password) { (result, error) in
             if let error = error {
               print(error)
                 return
             }
        
            onComplete()
        
           self.saveUserData(uid: result?.user.uid ?? "", email: email, username: username, password: password)
         }
    }
       
    func saveUserData(uid: String, email: String, username: String, password: String) {
       Firestore.firestore()
            .collection("users")
            .document(uid)
             .setData(
                   [
                       "id": uid,
                       "email": email,
                       "username": username,
                       "password": password,
                       "status": false
                   ]
            )
    }

    func signIn(email: String, password: String, onComplete: @escaping() -> Void) {
           Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
               if let error = error {
                   print(error.localizedDescription)
                   return
               }
               
               guard let uid = result?.user.uid else {
                   return
               }
                onComplete()
                self.loadUserData(uid: uid)
           }
    }
    
    func updateStatus(userId: String, status: Bool) {
        Firestore.firestore()
            .collection("users")
            .document(userId)
            .updateData(
                [
                    "status": status
                ]
            )
    }
    
    func getCurrentUser(onComplete: @escaping (User?) -> Void) {
        let userID = Auth.auth().currentUser?.uid
            Firestore.firestore()
                .collection("users")
                .document(userID!)
                .addSnapshotListener { (snapshot, error) in
                
                    if let error = error {
                        print(error.localizedDescription)
                       return
                    }
                    
                    let user = User(data: (snapshot?.data())!)
                    onComplete(user)
                }
    }
    
    func loadUserData(uid: String) {
        Firestore.firestore()
            .collection("users")
            .document(uid)
            .addSnapshotListener { (snapshot, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
            }
    }
    
    func getUsers(onComplete: @escaping ([User?]) -> Void) {
        Firestore.firestore()
            .collection("users")
            .addSnapshotListener { (snapshot, error) in
                if let error = error {
                    print(error.localizedDescription)
                    
                    return
                }
                
                var users = [User?]()
                
                for document in snapshot!.documents {
                    if document["id"] as! String != Auth.auth().currentUser!.uid {
                        users.append(User(data: document.data())!)
                    }
                }
                onComplete(users)
            }
    }
    
    func getChat(currentUserId: String, companionId: String, onComplete: @escaping (Chat?) -> Void) {
        Firestore.firestore()
            .collection("chats")
            .order(by: "lastMessageTimestamp", descending: true)
            .whereField("participantIds", arrayContains: currentUserId)
            .addSnapshotListener { (querySnapshot, error)  in
                if let error = error {
                    print(error.localizedDescription)
                    return
                } else {
                    var chats = [Chat?]()
                    for document in querySnapshot!.documents {
                        chats.append(Chat(data: document.data())!)
                    }
                    
                    self.checkCompanion(companionId: companionId, chats: chats) { (chat) in
                        onComplete(chat)
                    }
                }
            }
    }
    
    func checkCompanion(companionId: String, chats: [Chat?], onComplete: @escaping (Chat?) -> Void) {
        for chat in chats {
            if ((chat?.participantIds.contains(companionId))!) {
                onComplete(chat)
            }
        }
    }
    
    func getUserById(userId: String, onComplete: @escaping (User?) -> Void) {
        Firestore.firestore()
            .collection("users")
            .document(userId)
            .addSnapshotListener { (snapshot, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                let user = User(data: (snapshot?.data())!)
                onComplete(user)
              }
    }
    
    func getCurrentUserChats(currentUserId: String, onComplete: @escaping ([Chat?]) -> Void) {
        Firestore.firestore()
            .collection("chats")
            .order(by: "lastMessageTimestamp", descending: true)
            .whereField("participantIds", arrayContains: currentUserId)
            .addSnapshotListener { (querySnapshot, error)  in
                if let error = error {
                    print(error.localizedDescription)
                    return
                } else {
                    var chats = [Chat?]()
                    for document in querySnapshot!.documents {
                        if (!document.data().isEmpty) {
                            chats.append(Chat(data: document.data())!)
                        }
                    }
                    onComplete(chats)
                }
            }
    }
    
    func updateChat(chatId: String, lastMessage: String) {
        Firestore.firestore()
            .collection("chats")
            .document(chatId)
            .updateData(
                [
                    "lastMessage": lastMessage,
                    "lastMessageTimestamp": Timestamp()
                ]
            )
    }
    
    func createMessage(chatId: String, senderId: String, message: String) {
        Firestore.firestore()
            .collection("messages")
            .document().setData(
                [
                    "chatId": chatId,
                    "senderId": senderId,
                    "message": message,
                    "timestamp": Timestamp()
                ]
        )
    }
    
    func createChat(participantIds: [String], participants: [User?], lastMessage: String, onComplete: @escaping (String) -> Void) {
        let perticipantsDicts = participants.map({ $0?.toDict() })
        let reference = Firestore.firestore().collection("chats").document()
        let id = reference.documentID
        reference.setData(
            [
                "id": id,
                "participantIds": participantIds,
                "participants": perticipantsDicts,
                "lastMessage": lastMessage,
                "lastMessageTimestamp": Timestamp()
            ]
        ) { (error) in
            if error == nil {
                 onComplete(id)
            }
        }
    }
    
    func getMessage(chatId: String, onComplete: @escaping ([Message?]) -> Void) {
        Firestore.firestore()
            .collection("messages")
            .order(by: "timestamp", descending: false)
            .whereField("chatId", isEqualTo: chatId)
            .addSnapshotListener { (querySnapshot, error)  in
                if let error = error {
                    print(error.localizedDescription)
                    return
                } else {
                    var messages = [Message?]()
                    for document in querySnapshot!.documents {
                        if (!document.data().isEmpty) {
                            messages.append(Message(data: document.data())!)
                        }
                    }
                    onComplete(messages)
                }
            }
    }
    
    func signOut() -> Bool {
        do {
            try Auth.auth().signOut()
            return true
        } catch {
            return false
        }
    }
}
